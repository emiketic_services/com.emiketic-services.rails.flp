# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140525001728) do

  create_table "games", force: true do |t|
    t.datetime "date"
    t.string   "location"
    t.integer  "first_team_score"
    t.integer  "second_team_score"
    t.boolean  "tie"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "round_id"
    t.integer  "first_team_id"
    t.integer  "second_team_id"
    t.integer  "winner_id"
    t.integer  "league_id"
  end

  add_index "games", ["first_team_id"], name: "index_games_on_first_team_id"
  add_index "games", ["league_id"], name: "index_games_on_league_id"
  add_index "games", ["round_id"], name: "index_games_on_round_id"
  add_index "games", ["second_team_id"], name: "index_games_on_second_team_id"
  add_index "games", ["winner_id"], name: "index_games_on_winner_id"

  create_table "leagues", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sport_id"
  end

  add_index "leagues", ["sport_id"], name: "index_leagues_on_sport_id"

  create_table "picks", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ticket_id"
    t.integer  "game_id"
    t.integer  "team_id"
    t.integer  "round_id"
  end

  add_index "picks", ["game_id"], name: "index_picks_on_game_id"
  add_index "picks", ["round_id"], name: "index_picks_on_round_id"
  add_index "picks", ["ticket_id"], name: "index_picks_on_ticket_id"

  create_table "pool_histories", force: true do |t|
    t.integer  "pool_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pool_histories", ["pool_id"], name: "index_pool_histories_on_pool_id"

  create_table "pools", force: true do |t|
    t.string   "name"
    t.integer  "prize"
    t.integer  "cost"
    t.integer  "number_of_rounds"
    t.datetime "start_date"
    t.datetime "deadline"
    t.text     "overview"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "league_id"
    t.datetime "end_date"
    t.text     "recap"
    t.integer  "number_of_tickets"
    t.float    "margin"
    t.integer  "max_user_credit"
    t.boolean  "display_stat"
  end

  add_index "pools", ["league_id"], name: "index_pools_on_league_id"

  create_table "round_histories", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "round_id"
  end

  create_table "rounds", force: true do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "deadline"
    t.boolean  "with_games"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pool_id"
    t.string   "name"
    t.boolean  "complete"
    t.boolean  "display_stat", default: false
  end

  add_index "rounds", ["pool_id"], name: "index_rounds_on_pool_id"

  create_table "sports", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "game_id"
    t.integer  "league_id"
    t.string   "flag_code"
  end

  add_index "teams", ["game_id"], name: "index_teams_on_game_id"
  add_index "teams", ["league_id"], name: "index_teams_on_league_id"

  create_table "tickets", force: true do |t|
    t.integer  "value"
    t.datetime "date_of_sale"
    t.datetime "expiry_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pool_id"
    t.integer  "user_id"
    t.integer  "round_id"
    t.boolean  "validity"
  end

  add_index "tickets", ["pool_id"], name: "index_tickets_on_pool_id"
  add_index "tickets", ["round_id"], name: "index_tickets_on_round_id"
  add_index "tickets", ["user_id"], name: "index_tickets_on_user_id"

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.string   "country"
    t.string   "province"
    t.date     "dob"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "balance"
    t.datetime "last_login"
    t.integer  "last_deposit"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
  end

end
