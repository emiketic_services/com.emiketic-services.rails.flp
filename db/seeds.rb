# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Make sure all tables are clean in the database before seeding


require 'i18n_data'

Pool.delete_all
Round.delete_all
RoundHistory.delete_all
PoolHistory.delete_all
Game.delete_all
Ticket.delete_all
Pick.delete_all
User.delete_all
Team.delete_all
Sport.delete_all
League.delete_all

ActiveRecord::Base.connection.execute("ALTER TABLE games AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE leagues AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE sports AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE users AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE teams AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE tickets AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE pools AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE pool_histories AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE picks AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE rounds AUTO_INCREMENT = 1")

ActiveRecord::Base.connection.execute("ALTER TABLE round_histories AUTO_INCREMENT = 1")


############## Except for users DOB, all dates are expressed in datetime type ####################

### Sports ###
football = Sport.create!(name:'Football')
soccer = Sport.create!(name:'Soccer')
hockey = Sport.create!(name:'Hockey')
basketball = Sport.create!(name:'Basketball')


### Leagues ###
nfl = League.create!(name:'NFL')
nhl = League.create!(name:'NHL')
fifa_world = League.create!(name:'FIFA World Cup')
nba = League.create!(name:'NBA')
fifa_champions_league = League.create(name:'FIFA Champions League')

nfl.sport = football
nhl.sport = hockey
nba.sport = basketball
fifa_world.sport = soccer
fifa_champions_league.sport = soccer

nfl.save!
nhl.save!
fifa_world.save!
fifa_champions_league.save!
nba.save!

# Soccer World Cup teams
I18nData.countries.each do |country|
  if country.first == 'CI'
    team = Team.create(flag_code: country.first, name: 'Ivory Coast')
    team.league = fifa_world
    team.save!
  else
    team = Team.create(flag_code: country.first, name: I18nData.countries[country.first])
    team.league = fifa_world
    team.save!
  end

end

team = Team.create(flag_code: 'GB', name: 'England')
team.league = fifa_world
team.save!

### Users ###
=begin
steve = User.create!(balance: 3000,
             first_name: 'Steve',
             last_name: 'Sebag',
             username: 'ssebag',
             country: 'Canada',
             province: 'QC',
             phone: '514-1234-567',
             password: 'ssebag',
             dob: Date.new(1977, 5, 10),
             email: 'mehdi@appid.tn')
gay = User.create!(balance: 2000,
             first_name: 'Gay',
             last_name: 'Hazan',
             username: 'ghazan',
             country: 'Canada',
             province: 'QC',
             phone: '514-1234-567',
             password: 'ghazan',
             dob: Date.new(1976, 1, 15),
             email: 'support@appid.tn')
bernard = User.create!(balance: 4000,
             first_name: 'Bernard',
             last_name: 'Cohen',
             username: 'bcohen',
             country: 'Canada',
             province: 'QC',
             phone: '514-1234-567',
             password: 'bcohen',
             dob: Date.new(1979, 11, 12),
             email: 'support@appid.tn')
laurent = User.create!(balance: 450,
             first_name: 'Laurent',
             last_name: 'Sebag',
             username: 'lsebag',
             country: 'Canada',
             province: 'QC',
             phone: '514-1234-567',
             password: 'lsebag',
             dob: Date.new(1974, 6, 12),
             email: 'support@appid.tn')
=end






