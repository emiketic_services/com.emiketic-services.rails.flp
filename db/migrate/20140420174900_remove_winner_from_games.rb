class RemoveWinnerFromGames < ActiveRecord::Migration
  def up
    remove_column :games, :winner_id
  end

  def down
    add_column :games, :winner_id, :integer
  end
end
