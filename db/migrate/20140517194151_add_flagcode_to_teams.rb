class AddFlagcodeToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :flag_code, :string
  end
end