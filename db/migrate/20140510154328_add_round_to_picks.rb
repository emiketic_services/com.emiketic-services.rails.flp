class AddRoundToPicks < ActiveRecord::Migration
  def change
    add_reference :picks, :round, index: true
  end
end
