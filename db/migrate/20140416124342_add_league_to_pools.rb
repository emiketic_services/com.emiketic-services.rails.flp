class AddLeagueToPools < ActiveRecord::Migration
  def change
    add_reference :pools, :league, index: true
  end
end
