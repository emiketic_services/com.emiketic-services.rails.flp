class AddLastdepositandloginToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_login, :datetime
    add_column :users, :last_deposit, :integer
  end
end
