class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :value
      t.date :date_of_sale
      t.date :expiry_date
      t.boolean :valid

      t.timestamps
    end
  end
end
