class RemoveValidFromTickets < ActiveRecord::Migration
  def change
    remove_column :tickets, :valid
    add_column :tickets, :validity, :boolean
  end
end
