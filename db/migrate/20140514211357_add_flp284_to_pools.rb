class AddFlp284ToPools < ActiveRecord::Migration
  def change
    add_column :pools, :number_of_tickets, :integer
    add_column :pools, :margin, :float
    add_column :pools, :max_user_credit, :integer
  end
end
