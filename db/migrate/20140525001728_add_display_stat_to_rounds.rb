class AddDisplayStatToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :display_stat, :boolean, default: false
  end
end
