class AddEndDateToPools < ActiveRecord::Migration
  def change
    add_column :pools, :end_date, :date
  end
end
