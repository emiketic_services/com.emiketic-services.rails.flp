class AddTeamToPicks < ActiveRecord::Migration

  def up
    add_column :picks, :team_id, :integer, index: true
  end

  def change
    add_column :picks, :team_id, :integer, index: true
  end

  def down
    remove_column :picks, :team_id
  end
end
