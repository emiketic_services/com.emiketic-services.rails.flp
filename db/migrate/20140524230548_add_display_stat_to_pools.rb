class AddDisplayStatToPools < ActiveRecord::Migration
  def change
    add_column :pools, :display_stat, :boolean
  end
end
