class RemoveDetailsFromGames < ActiveRecord::Migration
  def up
    remove_column :games, :first_team
    remove_column :games, :second_team
  end

  def down
    add_column :games, :first_team, :integer
    add_column :games, :second_team, :integer
  end
end
