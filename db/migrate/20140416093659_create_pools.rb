class CreatePools < ActiveRecord::Migration
  def change
    create_table :pools do |t|
      t.string :name
      t.integer :prize
      t.integer :cost
      t.integer :number_of_rounds
      t.date :start_date
      t.date :deadline
      t.text :overview

      t.timestamps
    end
  end
end
