class AddDetailsToPools < ActiveRecord::Migration
  def change
    add_column :pools, :recap, :text
    change_column :pools, :start_date, :datetime
    change_column :pools, :end_date, :datetime
    change_column :pools, :deadline, :datetime
  end
end
