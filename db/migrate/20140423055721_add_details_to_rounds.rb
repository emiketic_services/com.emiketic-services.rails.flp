class AddDetailsToRounds < ActiveRecord::Migration
  def change
    change_column :rounds, :start_date, :datetime
    change_column :rounds, :end_date, :datetime
    change_column :rounds, :deadline , :datetime
    add_column :rounds, :name, :string
  end
end
