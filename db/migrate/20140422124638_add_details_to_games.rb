class AddDetailsToGames < ActiveRecord::Migration
  def change
    add_reference :games, :first_team, index: true
    add_reference :games, :second_team, index: true
    add_reference :games, :winner, index: true
  end
end
