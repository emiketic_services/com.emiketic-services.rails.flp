class AddDetailsToPicks < ActiveRecord::Migration
  def change
    add_reference :picks, :ticket, index: true
    add_reference :picks, :game, index: true
  end
end
