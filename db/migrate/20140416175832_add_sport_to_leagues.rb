class AddSportToLeagues < ActiveRecord::Migration
  def change
    add_reference :leagues, :sport, index: true
  end
end
