class AddChangesToTickets < ActiveRecord::Migration
  def change
    change_column :tickets, :date_of_sale, :datetime
    change_column :tickets, :expiry_date, :datetime
  end
end
