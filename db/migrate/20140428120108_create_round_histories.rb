class CreateRoundHistories < ActiveRecord::Migration
  def change
    create_table :round_histories do |t|
      t.integer :round

      t.timestamps
    end
    add_index :round_histories, :round
  end
end
