class AddDetailsToTickets < ActiveRecord::Migration

  def change
    add_reference :tickets, :pool, index: true
    add_reference :tickets, :user, index: true
    add_reference :tickets, :round, index: true
  end

end
