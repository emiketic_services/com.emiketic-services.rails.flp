class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.integer :first_team
      t.integer :second_team
      t.date :date
      t.string :location
      t.integer :winner_id
      t.integer :first_team_score
      t.integer :second_team_score
      t.boolean :tie

      t.timestamps
    end
  end
end
