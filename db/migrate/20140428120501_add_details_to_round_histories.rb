class AddDetailsToRoundHistories < ActiveRecord::Migration
  def change
    remove_column :round_histories, :round
    add_column :round_histories, :round_id, :integer, index: true
  end
end
