class CreatePoolHistories < ActiveRecord::Migration
  def change
    create_table :pool_histories do |t|
      t.integer :pool_id

      t.timestamps
    end
    add_index :pool_histories, :pool_id
  end
end
