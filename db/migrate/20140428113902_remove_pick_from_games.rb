class RemovePickFromGames < ActiveRecord::Migration

  def up
    remove_column :games, :pick_id
  end

  def change
    remove_column :games, :pick_id
  end

  def down
    add_column :games, :pick_id, :integer, index: true
  end
end
