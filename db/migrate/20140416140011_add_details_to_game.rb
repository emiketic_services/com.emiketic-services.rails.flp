class AddDetailsToGame < ActiveRecord::Migration
  def change
    add_reference :games, :round, index: true
    add_reference :games, :pick, index: true
  end
end
