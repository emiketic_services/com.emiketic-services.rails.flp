class AddPoolToRounds < ActiveRecord::Migration
  def change
    add_belongs_to :rounds, :pool, index: true
  end
end
