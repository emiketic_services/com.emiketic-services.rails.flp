require 'test_helper'

class UserNotifierTest < ActionMailer::TestCase
  test "registered" do
    mail = UserNotifier.registered
    assert_equal "Registered", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "registered_ticket" do
    mail = UserNotifier.registered_ticket
    assert_equal "Registered ticket", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "picked" do
    mail = UserNotifier.picked
    assert_equal "Picked", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "round_deadline_reminder" do
    mail = UserNotifier.round_deadline_reminder
    assert_equal "Round deadline reminder", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "pool_deadline_reminder" do
    mail = UserNotifier.pool_deadline_reminder
    assert_equal "Pool deadline reminder", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "deposit" do
    mail = UserNotifier.deposit
    assert_equal "Deposit", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "eliminated" do
    mail = UserNotifier.eliminated
    assert_equal "Eliminated", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
