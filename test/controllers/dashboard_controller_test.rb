require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  test "should get contests" do
    get :contests
    assert_response :success
  end

  test "should get my_tickets" do
    get :my_tickets
    assert_response :success
  end

  test "should get my_pools" do
    get :my_pools
    assert_response :success
  end

end
