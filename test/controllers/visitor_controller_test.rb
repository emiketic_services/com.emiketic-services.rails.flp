require 'test_helper'

class VisitorControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get pools" do
    get :pools
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get register" do
    get :register
    assert_response :success
  end

  test "should get rules" do
    get :rules
    assert_response :success
  end

  test "should get privacy" do
    get :privacy
    assert_response :success
  end

  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get signup" do
    get :signup
    assert_response :success
  end

end
