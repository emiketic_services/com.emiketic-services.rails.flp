# config valid only for Capistrano 3.5
lock '3.5.0'

set :application, 'flp'
set :repo_url, 'git@bitbucket.org:emiketic_services/com.emiketic-services.rails.flp.git'

set :branch, '1.3_ElliottChanges'

set :deploy_to, '/home/deploy/flp'

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}


namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end
