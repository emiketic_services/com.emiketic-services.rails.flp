# Application-wide date formatting
Date::DATE_FORMATS[:default] = "%B %e, %Y"     # November 3, 2013

# Application-wide datetime formatting
DateTime::DATE_FORMATS[:default] = "%B %e, %Y"     # November 3rd, 2013 14:22