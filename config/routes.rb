FLP::Application.routes.draw do

  ## Frontend and Dashboard rules ##
  root 'visitor#index', as: 'visitor'
  get '/admin' => 'visitor#admin_login'
  post '/admin' => 'visitor#admin_logout'
  post '/visitor/check_admin_login' => 'visitor#check_admin_login'
  get '/users/balances' => 'users#balances'
  put '/users/balances' => 'users#update_user_balance'

 namespace :visitor do

    get 'index' => :index
    get 'register' => :register
    get 'about' => :about
    get 'contact' => :contact
    get 'pools' => :pools
    get 'rules' => :rules
    get 'privacy' => :privacy
    get 'register_confirmation' => :register_confirmation
    get 'update_confirmation' => :update_confirmation
 end

  get "visitor/logout"
  post "visitor/login"
  post "visitor/create_password_reset_token", to: 'visitor#create_password_reset_token'
  post "visitor/update_password", to: 'visitor#save_password_update'
  get "visitor/password_reset"
  get "visitor/password_update", to: 'visitor#password_update'
  get "visitor/overview", to: 'visitor#overview'
  get "dashboard/contests"


  # Dispatches selected pool URL to my_pools method in the dashboard controller
  # required for refreshing my pools view when a pool is selected
  get "dashboard/my_pools", to: 'dashboard#my_pools'
  get "dashboard/my_pools/:id", to: 'dashboard#my_pools'

  # Dispatches selected ticket for a given pool URL to my_tickets method in the dashboard controller
  # required for refreshing my pools view when a ticket is selected
  get "dashboard/my_pools/ticket/:id", to: 'dashboard#update_selected_tickets'

  get "dashboard/my_tickets"

  get "dashboard/my_pools/picks/round/:id", to: 'dashboard#current_round'


  get "dashboard/overview", to: 'dashboard#overview'
  get "dashboard/overview/:id", to: 'dashboard#overview'
  get "dashboard/overview/pool/:id", to: 'dashboard#overview'
  get "dashboard/overview/round/:id", to: 'dashboard#overview'
  get "dashboard/schedule", to: 'dashboard#schedule'
  get "dashboard/schedule/:id", to: 'dashboard#schedule'
  post "dashboard/register_ticket" , to: 'dashboard#register_ticket_to_pool'
  get "dashboard/ticket_confirmation", to: 'dashboard#ticket_confirmation'
  get "dashboard/stat_recap", to: 'dashboard#stat_recap'
  get "dashboard/stat_recap/:id", to: 'dashboard#stat_recap'
  get "dashboard/stat_recap/pool/:id", to: 'dashboard#stat_recap'
  get "dashboard/stat_recap/round/:id", to: 'dashboard#stat_recap'
  get "dashboard/upcoming_pools"
  get "dashboard/current_pools"
  post "dashboard/save_pick", to: 'dashboard#save_pick'


  ## Backend routes ##

  resources :games

  resources :tickets do
    put :disable, on: :member
  end

  resources :picks

  resources :leagues

  resources :sports

  resources :teams

  resources :users do
    get :manage, on: :member
    put :update_managed_pick, on: :member
    post :add_managed_pick, on: :member
  end

  post "rounds/new", to: 'rounds#new'

  resources :rounds do
    get :history, on: :member
    put :complete, on: :member
    post :display_stat, on: :member
  end

  resources :pools do
    get :manage, on: :member
    get :stat_recap, on: :member
    post :display_stat, on: :member
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
