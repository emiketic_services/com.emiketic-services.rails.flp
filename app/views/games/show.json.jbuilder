json.extract! @game, :id, :first_team, :second_team, :date, :location, :winner_id, :first_team_score, :second_team_score, :tie, :created_at, :updated_at
