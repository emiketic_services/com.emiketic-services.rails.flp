json.array!(@games) do |game|
  json.extract! game, :id, :first_team, :second_team, :date, :location, :winner_id, :first_team_score, :second_team_score, :tie
  json.url game_url(game, format: :json)
end
