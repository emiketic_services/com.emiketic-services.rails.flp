json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :value, :date_of_sale, :expiry_date, :valid
  json.url ticket_url(ticket, format: :json)
end
