json.array!(@pools) do |pool|
  json.extract! pool, :id, :name, :prize, :cost, :number_of_rounds, :start_date, :deadline, :overview
  json.url pool_url(pool, format: :json)
end
