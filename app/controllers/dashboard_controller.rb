# Instance variables: @pools, @my_pools, @my_tickets, @my_tickets_collection,
# @selected_pool, @selected_ticket

class DashboardController < ApplicationController
  skip_before_action :authorize
  before_action :set_pool, only: [:overview, :register_ticket_to_pool, :ticket_confirmation, :my_pools]
  before_filter :current_user
  before_filter :version_details
  before_action :all_tickets, only: [:my_pools, :update_selected_tickets, :overview, :stat_recap]
  before_action :contests, only: [:overview, :stat_recap, :schedule]
  #before_action :my_pools, only: [:overview, :schedule, :stat_recap]

  layout "dashboard"

  def contests
   @pools = Pool.all
  end


  # Gets all tickets for user. Used to retrieve pools in which she is registered through tickets.
  # Request isn't asking for a particular pool, i.e., /my_pools or this is the first time my_pools
  # is requested
  def all_tickets
    @my_tickets_collection = @current_user.tickets.order(:expiry_date)

    # Mark expired tickets as invalid if they haven't paid before deadline
    # Note: The all_tickets function needs to be called at least once for the ticket to be invalidated. Hence, it should
    # be invoked before any function involving tickets in the dashboard
    @my_tickets = {}
    @my_tickets_collection.each_with_index do |ticket, index|
      @my_tickets[index+1] = ticket


      # Disable ticket eliminitation when pool is past deadline for FIFA WorldCup
=begin
      if (ticket.expiry_date == ticket.pool.deadline) && (ticket.validity) && (DateTime.current >= ticket.pool.deadline)
        ticket.validity = false
        # Save and notify user that her ticket was eliminated the first time she logs in to the dashboard
        # Note: This is a temporary solution until task scheduling is implemented
        if ticket.save
          UserNotifier.eliminated(ticket).deliver
        end
      end
=end
    end

  end

  # Gets tickets implicitly from the selected pool. @selected_pool is assumed to be valid
  # Selected pool is already set, which means coming from a /my_pools/id request
  def update_selected_tickets

    # Set active ticket instance variable for rendering
    if session[:selected_ticket]
      @active_ticket = Ticket.find_by(id: session[:selected_ticket])
    else
      if @my_tickets.empty?
        @my_pools = nil
      else
        @active_ticket = @my_tickets_collection.first
        session[:selected_ticket] = @active_ticket.id
      end
    end

    tickets_collection_for_pool =
        @current_user.tickets.where(pool_id: session[:selected_pool]).order(:expiry_date)

    @my_tickets = {}
    tickets_collection_for_pool.each_with_index do |ticket, index|
      @my_tickets[index+1] = ticket
    end

    # Get the selected ticket from URL or assign first ticket as default ticket for pool
    if request.fullpath.include?('/ticket/')

      # Look for the selected ticket in there
      selected_ticket_index ||= params[:id].to_i
      if params[:id]
        session[:selected_ticket] =  @my_tickets[selected_ticket_index].id
        session[:selected_ticket_index] = selected_ticket_index
      else
        session[:selected_ticket] = @my_tickets[1].id
        session[:selected_ticket_index] = 0
      end

      respond_to do |format|
        format.html { redirect_to controller: :dashboard,
                                  action: :my_pools,
                                  id: session[:selected_pool_index] }
      end

    end



  end

  def my_pools

    @browser = Browser.new(:ua => request.user_agent)

    # Instance container for user pools
    @my_pools = []

    if @my_tickets.empty?
      @my_pools = nil
    else

      # Retrieve pools from user tickets
      @my_tickets_collection.each do |ticket|
        unless @my_pools.include?(ticket.pool)
          @my_pools << ticket.pool
        end
      end

      # Sort pools, if any, by id
      @my_pools.sort_by {|o| o[:id]} unless @my_pools.empty?

      # Get the selected pool from URL or assign first pool as default pool
      # When my_pools is triggered by a pools drop-down list select change or round change
      if (!request.fullpath.include?('/ticket/')) && (!request.fullpath.include?('/round/'))
        selected_pool_index ||= params[:id].to_i
        if params[:id]
          # Check if pool is different from current pool, if so reset the current round index
          # so that it can be set properly to the first round in the pool
          if session[:selected_pool] != @my_pools.at(selected_pool_index).id
            if session[:selected_pool] == nil
              session[:selected_pool] = @my_pools.first.id
            end
            session[:current_round] = nil
          end
          session[:selected_pool] = @my_pools.at(selected_pool_index).id
          session[:selected_pool_index] = selected_pool_index
        else
          # If a pool is already selected, example coming from ticket confirmation page
          if session[:selected_pool]
            # Retrieve its index from the my_pools collection (user exploring her pools) or set the selector
            # to the first user pool in the drop-down (user was exploring other pools that are not in my pools)
            if @my_pools.include?(Pool.find(session[:selected_pool]))
              session[:selected_pool_index] =  @my_pools.index(session[:selected_pool])
            else
              session[:selected_pool_index] = 0
              session[:selected_pool] = @my_pools.first.id
            end


          # Otherwise select fist pool in my pools list
          else
            session[:selected_pool] = @my_pools.first.id
            session[:selected_pool_index] = 0
          end

        end

        @active_pool = Pool.find_by(id: session[:selected_pool])

        # Update ticket collection to be displayed for the selected pool
        update_selected_tickets

        current_round unless session[:change_round]
      end


      # When this block is reached, my_pools was requested by either a regular my_pools request
      # or after a redirection upon updating current round by the current_round method
      # In both cases, any processing on the @current_round instnace mus be done after this block
      if (session[:current_round] != nil) && (session[:change_round] == true)
        @current_round = Round.find_by(id: session[:current_round])
        session[:change_round] = false
      elsif session[:current_round].nil?
        if Pool.find_by(id: session[:selected_pool]).rounds.empty?
          @current_round = nil
          return
        else
          first_round_id = Pool.find_by(id: session[:selected_pool]).rounds.first.id
          @current_round = Round.find_by(id: first_round_id)
        end
      elsif !session[:change_round]
        @current_round = Round.find_by(id: session[:current_round])
        puts @current_round.id
      end

      update_possible_picks

    end

  end

  def current_round
    @active_pool ||= Pool.find_by(id: session[:selected_pool])
    @current_round = nil

    # Request coming from clicking round div in pool view
    if request.fullpath.include?('/round/')
      unless @active_pool.rounds.empty?
        @current_round = @active_pool.rounds.order(:start_date).at(params[:id].to_i)
        session[:current_round] = @current_round.id
        session[:current_round_index] = params[:id]
        session[:change_round] = true
        if params[:current_view] == "schedule"
          respond_to do |format|
            format.html { redirect_to controller: :dashboard,
                                      action: :schedule,
                                      id: session[:selected_pool_index] }
          end
        else
          respond_to do |format|
            format.html { redirect_to controller: :dashboard,
                                      action: :my_pools,
                                      id: session[:selected_pool_index] }
          end
        end
      end



    # Called by my_pools to update current round
    else
      if @active_pool.rounds.blank?
        @current_round = nil
      else
        if session[:current_round].nil?
          @current_round = @active_pool.rounds.order(:start_date).first
          session[:current_round] = @current_round.id
        else
          @current_round = Round.find_by(id: session[:current_round])
        end

      end
    end

  end

  def upcoming_pools
    @pools= Pool.all
    @future_pools= @pools.where("start_date > ?", Date.today)
  end
  def current_pools
    @pools= Pool.all
    @current_pools= @pools.where("start_date < ? AND end_date > ? ", Date.today , Date.today)
  end

  def overview
    @browser = @browser = Browser.new(:ua => request.user_agent)
    session[:current_view] = "overview"

    if request.fullpath.include?('/pool/')
       # Get a pool from the pools collection by its id (equivalent to index since elements are sorted by index in .all)
       selection_index = params[:id].to_i
       @active_pool = @pools.at(selection_index)
       session[:selected_pool] = @active_pool.id
    else
       if params[:upcoming_pool]
         @active_pool = Pool.find_by(id: params[:upcoming_pool].to_i)
         session[:selected_pool] = @active_pool.id
       else
         if session[:selected_pool]
           @active_pool = Pool.find(session[:selected_pool])
         else
           @active_pool = @pools.at(0)
           session[:selected_pool] = @active_pool.id
         end
       end

    end
  end

  def schedule
    session[:current_view] = "schedule"

    @my_pools = Pool.all

    if params[:id]
      session[:selected_pool] = @my_pools.at(params[:id].to_i).id
      @active_pool = @my_pools.at(params[:id].to_i)
    end

    # If a pool is already selected, exemple coming from ticket confirmation page
    if session[:selected_pool]
      # Retrieve its index from the my_pools collection (user exploring her pools) or set the selector
      # to the first user pool in the drop-down (user was exploring other pools that are not in my pools)
      if @my_pools.include?(Pool.find(session[:selected_pool]))
        session[:selected_pool_index] =  @my_pools.index(session[:selected_pool])
        selected_pool = Pool.find(session[:selected_pool])
        @active_pool = selected_pool
        if session[:current_round]
          if selected_pool.rounds.any?
            @current_round = Round.find_by(id: session[:current_round])
          else
            @current_round = nil
          end
        else
          if selected_pool.rounds.any?
            @current_round = selected_pool.rounds.first
            session[:current_round] = @current_round.id
          else
            @current_round = nil
          end
        end
      else
        session[:selected_pool_index] = 0
        session[:selected_pool] = @my_pools.first.id
      end


      # Otherwise select fist pool in my pools list
    else
      session[:selected_pool] = @my_pools.first.id
      session[:selected_pool_index] = 0
    end

  end
  def stat_recap
    @browser = @browser = Browser.new(:ua => request.user_agent)
    # User by the handle_selection method in dashboard.js to manage dropdown select events
    session[:current_view] = "stat_recap"

    # Find out which button was clicked in the stat recap page to determine what report to generate
    if params[:display_report]
      @display_report = params[:display_report]
    else
      @display_report = "recap"
    end

    if request.fullpath.include?('/pool/')
      # Get a pool from the pools collection by its id (equivalent to index since elements are sorted by index in .all)
      selection_index = params[:id].to_i
      @active_pool = @pools.at(selection_index)
      session[:selected_pool] = @active_pool.id
    elsif request.fullpath.include?('/round/')
      # Rounds
    else
      if session[:selected_pool]
        @active_pool = Pool.find(session[:selected_pool])
      else
        @active_pool = @pools.at(0)
        session[:selected_pool] = @active_pool.id
      end

    end

    # Teams in pool
    if @active_pool and @active_pool.rounds.any?
      @teams_in_pool = []
      @active_pool.rounds.each do |round|
        round.games.each do |game|
          first_team ||= Team.find(game.first_team_id).name
          @teams_in_pool << first_team unless @teams_in_pool.include?(first_team)
          if round.with_games
            second_team ||= Team.find(game.second_team_id).name
            @teams_in_pool << second_team unless @teams_in_pool.include?(second_team)
          end
        end
      end
    end

  end

  def my_account

  end

  def ticket_confirmation
    @purchased_ticket = @current_user.tickets.last
  end

  def register_ticket_to_pool

    ticket  = nil

    # When user has enough balance for the pool cost, ticket is set to expire by the end of the pool
    # and is valid from now
    if @current_user.balance >= @active_pool.cost
      ticket = Ticket.new(date_of_sale: DateTime.current,
                          expiry_date: @active_pool.end_date,
                          value: @active_pool.cost, validity: true)

    # Otherwise, user is allowed access to dashboard until a certain deadline after which the ticket
    # will be invalid
    else
      ticket = Ticket.new(date_of_sale: DateTime.current,
                          expiry_date: @active_pool.deadline,
                          value: @active_pool.cost, validity: true)
    end

    # In both cases set the ticket as belonging to user for this pool
    ticket.user = @current_user
    ticket.pool = @active_pool

    # Set its round to the first round in the pool if any
    if @active_pool.rounds.empty?
      ticket.round = nil
    else
      ticket.round_id = @active_pool.rounds.first.id
    end

    # Redirect to overview in case of an error or carry user to my_pools otherwise
    respond_to do |format|
      if ticket.save
        # Take the ticket amount out of user balance
        @current_user.balance -= @active_pool.cost
        @current_user.save

        # Reset session variables for pool in dashboard
        session[:change_round] = nil
        if @active_pool.rounds.empty?
          session[:current_round] = nil
        else
          session[:current_round] = @active_pool.rounds.first.id
        end

        # Notify user
        #UserNotifier.registered_ticket(@current_user, ticket).deliver

        format.html {redirect_to dashboard_ticket_confirmation_path,
                                 flash: {success: true}}
        format.json
      else
        format.html {redirect_to dashboard_ticket_confirmation_path,
                                 flash: {success: false}}
      end
    end

  end

  def update_possible_picks

    # Update the user's pick choice for this round. This will be used to populate the radio buttons group.
    @possible_picks = []


    @active_ticket ||= Ticket.find_by(id: session[:selected_ticket])
    @current_round = Round.find_by(id: session[:current_round])


    # Display pick for this round if round is complete
    if @current_round

      # Past picks
      @past_picks = []
      if @active_ticket
        unless @active_ticket.picks.count == 0
          @active_ticket.picks.order(:created_at).each do |past_pick|
            unless (past_pick.round == @current_round) || (past_pick.round.start_date >= @current_round.start_date)
              @past_picks << past_pick.team_id
            end
          end
        end

      end

      # Completed round pick
      if @current_round.complete && @active_ticket.validity
        unless Team.find_by(id:@active_ticket.picks.where(round_id: @current_round.id)).blank?
          @completed_round_pick =
              Team.find_by(id:@active_ticket.picks.where(round_id: @current_round.id).first.team_id).name
        end
      else
        if @current_round.games.present?

          # For every game in round
          @current_round.games.each do |game|

            # Make sure first team association id is not present in the @possible_picks array
            # Also make sure user can't select same team twice for survival
            unless @possible_picks.include?(game.first_team_id) || @past_picks.include?(game.first_team_id)
              @possible_picks << game.first_team_id
            end

            # Then, add the second team if it doesn't exist neither for the case of dual-game based rounds
            if @current_round.with_games
              unless @possible_picks.include?(game.second_team_id)|| @past_picks.include?(game.second_team_id)
                @possible_picks << game.second_team_id
              end
            end
          end

          # Display current pick for this round in radio button group
          @current_pick_index = -1

          unless @active_ticket.picks.blank?
            picked_team = @active_ticket.picks.where(round_id: @current_round.id).first.team_id if
                @active_ticket.picks.where(round_id: @current_round.id).any?
            @current_pick_index = @possible_picks.index(picked_team)
          end

        end
      end
    else
      @possible_picks = nil
      @completed_round_pick = nil
    end


  end

  def save_pick

    # Init instance variables from session variables along with possible picks collection
    update_possible_picks
    pick = nil

    # Retrieve team id from possible team collection based on the radio button group pick index
    if params[:ticket_picks] && DateTime.current < @current_round.deadline
      pick_index = params[:ticket_picks].to_i
      picked_team = @possible_picks.at(pick_index)

      # Make sure pick choice has changed
      unless @active_ticket.picks.where(round_id: @current_round.id).include?(picked_team)

        if @active_ticket.picks.where(round_id: @current_round.id).any?
          # Update any existing picks
          pick = Pick.find_by(id: @active_ticket.picks.where(round_id: @current_round.id).first.id)
        else
          # Or Create a new pick for the current ticket and round
          pick = Pick.new
          pick.ticket = @active_ticket
          pick.round = @current_round

        end

        # Update or assign team for new pick in both cases
        pick.team_id = Team.find_by(id: picked_team).id

        # For practical ulterior reasons, update the ticket's round to the last pick round
        # we do not pay much attention to the saving assertion since this is not an essential operation
        @active_ticket.round = @current_round
        @active_ticket.save
      end
    end

    # When user picked at least a team that exists in current round
    if pick.present?
      if pick.save

        # Notify user
        #UserNotifier.picked(@current_user, pick).deliver

        respond_to do |format|
          format.html {redirect_to dashboard_my_pools_path,
                                   flash: {notice: 'Updated pick successfully', success: true}}
        end
      else
        respond_to do |format|
          format.html {redirect_to dashboard_my_pools_path,
                                   flash: {notice: 'Could not save your pick for this round', success: false}}
        end
      end

    # Redirect to my pools with an error notice if user hasn't picked any team
    else
      respond_to do |format|
        format.html {redirect_to dashboard_my_pools_path,
                                 flash: {notice: 'Please pick a team for this round', success: false}}
      end
    end
  end

  private

  def current_user
    if session[:user_id].nil?
      respond_to do |format|
        format.html {redirect_to visitor_path,
                            flash:{notice: "You are not logged in. Please login or sign-up to Fat Lady Pools first.",
                                   success: false}}
      end
    else
      @current_user ||= User.find(session[:user_id])
    end
  rescue ActiveRecord::RecordNotFound
    session[:user_id] = nil
    respond_to do |format|
      format.html {redirect_to visitor_path,
                               flash:{notice: "You are not logged in. Please login or sign-up to Fat Lady Pools first.",
                                      success: false}}
    end

  end

  def set_pool
    @active_pool ||= Pool.find_by(id: session[:selected_pool])
  end

  def version_details
    @version_details = `git describe`
  end

end
