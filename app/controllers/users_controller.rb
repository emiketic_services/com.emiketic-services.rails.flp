class UsersController < ApplicationController
  skip_before_action :authorize, only: [:new, :create, :edit, :update]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :manage, :add_managed_pick, :update_managed_pick]
  layout "backend"

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  def manage

  end

  def add_managed_pick

    # Parse information required for update
    team ||= Team.find(params[:team_id].to_i)
    ticket ||= Ticket.find(params[:ticket].to_i)
    round ||= Round.find(params[:round].to_i)

    # Do not update pick if drop down list selection hasn't changed
    unless ticket.picks.where(round_id: round.id).any?
        pick = Pick.new
        pick.ticket = ticket
        pick.team_id = team.id
        pick.round = round
        if pick.save
          # Notify user
          #UserNotifier.picked(@user, pick).deliver
          respond_to do |format|
            format.html {redirect_to manage_user_path(@user),
                                     flash: {notice: "Added pick #{team.name} for user #{@user.username} successfully",
                                             success: true}}
          end
        else
          respond_to do |format|
            format.html {redirect_to manage_user_path(@user),
                                     flash: {notice: "Could not add pick for user #{@user.username}",
                                             success: false}}
          end
        end
    end

  end

  def update_managed_pick

    if params[:commit] == 'Update'
      # Parse information required for update
      team ||= Team.find(params[:team_id].to_i)
      ticket ||= Ticket.find(params[:ticket].to_i)
      round ||= Round.find(params[:round].to_i)

      # Do not update pick if drop down list selection hasn't changed
      unless ticket.picks.where(round_id: round.id).blank? || team.nil?
        if ticket.picks.where(round_id: round.id).first.team_id != team.id
          pick = Pick.find(ticket.picks.where(round_id: round.id).first.id)
          pick.team_id = team.id
          if pick.save
            # Notify user
            #UserNotifier.picked(@user, pick).deliver
            respond_to do |format|
              format.html {redirect_to manage_user_path(@user),
                                       flash: {notice: "Updated pick to #{team.name} for user #{@user.username} successfully",
                                               success: true}}
            end
          else
            respond_to do |format|
              format.html {redirect_to manage_user_path(@user),
                                       flash: {notice: "Could not update pick for user #{@user.username}",
                                               success: false}}
            end
          end
        else
          respond_to do |format|
            format.html {redirect_to manage_user_path(@user),
                                     flash: {notice: "Nothing to update for this ticket",
                                             success: true}}
          end
        end
      end
    elsif params[:commit] == 'Remove'
      # Parse information required for update
      team ||= Team.find(params[:team_id].to_i)
      ticket ||= Ticket.find(params[:ticket].to_i)
      round ||= Round.find(params[:round].to_i)

      # Do not remove empty picks
      unless ticket.picks.where(round_id: round.id).blank? || team.nil?
        if ticket.picks.where(round_id: round.id).first.team_id == team.id
          pick = Pick.find(ticket.picks.where(round_id: round.id).first.id)
          if pick.destroy
            respond_to do |format|
              format.html {redirect_to manage_user_path(@user),
                                       flash: {notice: "Removed pick (#{team.name}) for user #{@user.username} successfully",
                                               success: true}}
            end
          else
            respond_to do |format|
              format.html {redirect_to manage_user_path(@user),
                                       flash: {notice: "Could not remove pick for user #{@user.username}",
                                               success: false}}
            end
          end
        else
          respond_to do |format|
            format.html {redirect_to manage_user_path(@user),
                                     flash: {notice: "Could not remove pick for user #{@user.username}: ID mismatch",
                                             success: false}}

          end
        end
      end
    end

  end

  def balances
    @users = User.all
  end

  def update_user_balance

    # Get user from table and credit/debit its balance from amount field value
    @user ||= User.find_by(id: params[:user_id].to_i)
    unless @user.nil?
      @user.balance += params[:amount].to_i
      @user.last_deposit = params[:amount].to_i
    end


    respond_to do |format|
      if @user.save
        #UserNotifier.deposit(@user).deliver
        format.html {redirect_to users_balances_path,
                                 flash: {notice: "User balance updated successfully for #{@user.username}",
                                        success: true}}
        format.json
      else
        format.html {redirect_to users_balances_path,
                                 flash: {notice: "Could not update balance for #{@user.username}",
                                         success: false}}
      end

    end

  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.balance = 0

    respond_to do |format|
      if @user.save

        # Notify user
        #UserNotifier.registered(@user).deliver

        # Admin added user from backend
        if session[:user_id]
          format.html { redirect_to @user, flash: {notice: 'User was successfully added',
                                                   success: true} }
          format.json { render action: 'show', status: :created, location: @user }
        else
          session[:user_id] = @user.id
          format.html { redirect_to visitor_register_confirmation_path}
          format.json { render action: 'show', status: :created, location: @user }
        end

      # User registered himself from front-end
      else
        if session[:user_id]
          format.html { render action: 'new', flash: {notice: 'Could not add user',
                                                      success: false} }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        else
          session[:user_id] = nil
          format.html { render 'visitor/register',
                               layout: 'frontend',
                               flash: {notice: 'There was a problem creating your account, please retry',
                                                           success: false} }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end

      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update

    @user.update(user_params)

    respond_to do |format|
      if @user.save

        # Notify user
        #UserNotifier.registered(@user).deliver

        # Admin added user from backend
        if session[:admin_logged]
          format.html { redirect_to @user, flash: {notice: 'User was successfully updated',
                                                   success: true} }
          format.json { render action: 'show', status: :created, location: @user }

        # User registered himself from front-end
        else
          session[:user_id] = @user.id
          format.html { redirect_to visitor_update_confirmation_path}
          format.json { render action: 'show', status: :created, location: @user }
        end


      else
        if session[:admin_logged]
          format.html { render action: 'new', flash: {notice: 'Could not add user',
                                                      success: false} }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        else
          session[:user_id] = @user.id
          format.html { render 'visitor/register',
                               layout: 'frontend',
                               flash: {notice: 'There was a problem updating your account, please retry',
                                       success: false} }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end

      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :first_name, :last_name, :email, :phone, :country, :province, :dob)
    end
end
