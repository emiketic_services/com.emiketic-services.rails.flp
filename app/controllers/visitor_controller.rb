class VisitorController < ApplicationController

  skip_before_action :authorize
  before_filter :version_details

  layout "frontend"

  def index
    @pools = Pool.all
    @future_pools = @pools.where("start_date > ?", Date.today)
    @current_pools = @pools.where("start_date <= ?", Date.today)
  end

  def about
  end

  def pools
  end

  def contact
  end

  def overview
    @overview_pool ||= Pool.find(params[:id])
  end

  def register
    # Check whether signup form was called from My Tickets > Edit
    if params[:render_edit]
      @user ||= User.find(session[:user_id].to_i)
    else
      # Make sure any user is logged out before proceeding to registration
      session[:user_id] = nil
      @user = User.new
    end
  end

  def register_confirmation
    # Retrieve user from the newly created instance that was saved in the session hash
    @user = User.find_by(id: session[:user_id])
  end

  def update_confirmation
    # Retrieve user from the newly created instance that was saved in the session hash
    @user = User.find_by(id: session[:user_id])
  end

  def rules
  end

  def privacy
  end

  def admin_login

  end

  def check_admin_login
    if params[:login] == 'BertAdmin' && params[:password] == '7u4zX5Rb'
      session[:admin_logged] = true
      respond_to do |format|
        format.html {redirect_to pools_path, flash: {notice: 'Admin Logged In', success: true}}
      end
    else
      session[:admin_logged] = false
      respond_to do |format|
        format.html {redirect_to '/admin', method: :get, flash: {notice: 'Invalid Credentials', success: false}}
      end
    end
  end

  def admin_logout
    session[:admin_logged] = false
    respond_to do  |format|
      format.html {redirect_to '/admin', method: :get, flash: {notice: 'Admin Logged Out', success: true}}
    end
  end

  def login

    # Log user out from any other session she might be logged in
    # without redirecting her to logout confirmation page
    logout false

    user = User.find_by(username: params[:username])
    if user and params[:password] == user.password
      session[:user_id] = user.id
      user.last_login = DateTime.current
      user.save
      redirect_to dashboard_contests_path, flash: {notice: "Logged in", success: true}
    else
    redirect_to visitor_index_path, notice: 'Invalid Password'
    end
  end

  def logout(with_redirect = true)

    # Clear dashboard remaining session variables
    clear_dashboard_session

    session[:user_id] = nil

    if with_redirect
      respond_to do |format|
        format.html { redirect_to visitor_index_path, flash: {notice: 'Logged out' ,success: true}}
      end
    end
  end

  def create_password_reset_token

    @user ||= User.find_by(email: params[:email])

    if @user
      @user.password_reset_token = SecureRandom.urlsafe_base64
      @user.password_reset_sent_at = DateTime.current
      if @user.save
        #UserNotifier.reset_password(@user).deliver
        respond_to  do  |format|
          format.html {redirect_to visitor_index_path,
                                   flash: {notice: 'We received your password reset request. You will shortly receive an email with instructions to reset your password.',
                                           success: true}}
        end
      else
        respond_to do |format|
          format.html {redirect_to visitor_path,
                                   flash: {notice: 'We could not reset your password. Please try again.',
                                           success: false}}
        end
      end
    else
      respond_to do |format|
        format.html {redirect_to visitor_path,
       flash: {notice: 'There are no users matching the email address you entered in our records. Please verify your e-mail address',
                                         success: false}}
      end
    end
  end

  def password_update
    @expired = true
    @user ||= User.find_by(password_reset_token: params[:format])
    if @user
      @expired = (@user.password_reset_sent_at < 1.hours.ago) ? true : false
    end
  end

  def save_password_update
    @user = User.find(params[:user].to_i)
    if params[:password] == params[:password_confirmation]
      @user.password = params[:password]
      if @user.save
        respond_to do |format|
          format.html {redirect_to visitor_path,
                                   flash: {notice: 'Your password was successfully updated. Please login again.',
                                           success: true}}
        end
      else
        respond_to do |format|
          format.html {redirect_to visitor_password_update_path(@user.password_reset_token),
                                   flash: {notice: 'Your password could not be updated.
It must be at least 6 characters long and include one number and one letter. Please try again.',
                                           success: false}}
        end
      end
    else
      respond_to do |format|
        format.html {redirect_to visitor_password_update_path(@user.password_reset_token),
                                 flash:{notice: 'Password Mismatch: Passwords must be identical', success: false}}
      end
    end
  end

  def version_details
    @version_details = `git describe`
  end

  private

    # Clears user dashboard variables
    def clear_dashboard_session

      session[:selected_ticket] = nil
      session[:selected_ticket_index] = nil
      session[:selected_pool] = nil
      session[:selected_pool_index] = nil
      session[:change_round] = nil
      session[:current_round] = nil
    end

end
