class GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :update, :destroy]
  layout "backend"

  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end

  # GET /games/new
  def new
    @game = Game.new
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)

    respond_to do |format|

      # Send a JS representation of the game update in case of an AJAX call (partial)
      # or a complete HTML page otherwise (show action for now)
      if @game.save
        #format.js {render 'rounds/acknowledge.js.erb'}
        format.html {  }
        format.json { render action: 'show', status: :created, location: @game }
      else
        #format.js
        format.html {  }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update

    respond_to do |format|

      # For the case of dual games,
      # save updated game scores only if a winner_select radio button is specified
      if params['winner_select']
        @game.first_team_score = params[:game][:first_team_score]
        @game.second_team_score = params[:game][:second_team_score]

        # Keep the winner id as nil if there's a tie, otherwise associate it to one of the teams
        if params['winner_select'] == 'tie'
          @game.winner_id = nil
        else
        @game.winner_id = (params['winner_select'] == 'first_team') ? @game.first_team_id : @game.second_team_id
        end
      else
        # Make sure this is a qualification round and not dual game round without filled radio buttons
        # The winner id is assigned by default to the teams in the qualifcations round. Set it to nil
        # when they are eliminated
        unless @game.round.with_games
          @game.winner_id = nil
        end
      end

      if @game.save
        #format.js {render 'rounds/acknowledge.js.erb'}
        format.html { redirect_to manage_pool_path(session[:pool_id]),
                                  flash: {notice: 'Updated game successfully',
                                          success: true} }
        format.json { render action: 'show', status: :created, location: @game }
      else
        #format.js
        format.html { redirect_to manage_pool_path(session[:pool_id]),
                                  flash: {notice: 'Could not update game',
                                          success: false} }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:first_team, :second_team, :date, :location, :winner_id, :first_team_score, :second_team_score, :tie)
    end
end
