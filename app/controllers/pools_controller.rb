class PoolsController < ApplicationController
  before_action :set_pool, only: [:show, :edit, :update, :destroy, :manage, :stat_recap, :display_stat]
  layout "backend"



  # GET /pools
  # GET /pools.json
  def index
    @pools = Pool.all
    session[:pool_id] = nil

    # Send a JS representation of the Pool index in case of an AJAX call (partial)
    # or a complete HTML page otherwise (show action for now)
    respond_to do |format|
      format.js
      format.html
    end
  end

  # GET /pools/1/manage
  def manage
    @pool = Pool.find_by(id: session[:pool_id])
  end

  def stat_recap

  end

  def display_stat
    success_notice_message =
        @pool.display_stat ? 'Stats are now hidden from users in dashboard' :
            'Stats are now published to users in dashboard'
    error_notice_message =
        @pool.display_stat ? 'Could not hide stats from users in dashboard' :
            'Could not publish stats to users on in dashboard'
    @pool.display_stat = !@pool.display_stat

    if @pool.save
      respond_to do |format|
        format.html {redirect_to manage_pool_path(@pool),
                                 flash:{notice: success_notice_message,
                                        success: true}}
      end
    else
      respond_to do |format|
        format.html {redirect_to manage_pool_path(@pool),
                                 flash:{notice: error_notice_message,
                                        success: true}}
      end
    end

  end

  # GET /pools/1
  # GET /pools/1.json
  def show
  end

  # GET /pools/new
  def new
    @pool = Pool.new
  end

  # GET /pools/1/edit
  def edit
  end

  # POST /pools
  # POST /pools.json
  def create

    @pool = Pool.new

    @pool.name = params[:pool][:name]
    @pool.league_id = params[:pool][:league_id].to_i
    @pool.cost = params[:pool][:cost].to_i
    @pool.prize = params[:pool][:prize].to_i
    @pool.number_of_tickets = params[:pool][:number_of_tickets].to_i
    @pool.max_user_credit = params[:pool][:max_user_credit].to_i
    number_of_rounds =  params[:pool][:number_of_rounds].to_i
    @pool.number_of_rounds = number_of_rounds
    @pool.start_date = datetime_from_params_hash(params[:pool], :start_date)
    @pool.end_date = datetime_from_params_hash(params[:pool], :end_date)
    @pool.deadline = datetime_from_params_hash(params[:pool], :deadline)
    @pool.overview = params[:pool][:overview]



    respond_to do |format|
      if @pool.save

        if number_of_rounds > 0
          # Create a default empty qualification round
          qualifications =  Round.new
          qualifications.with_games = false
          qualifications.pool = @pool
          qualifications.start_date = @pool.start_date + 2.days
          qualifications.end_date = @pool.start_date + 5.days
          qualifications.deadline = @pool.start_date + 1.day
          qualifications.save
          qualification_history  = RoundHistory.new
          qualification_history.round = qualifications
          qualification_history.save

          # Plus a number of dual-game rounds corresponding to the pool number_of_rounds - 1
          (number_of_rounds-1).times do |index|
            round = Round.new
            round.with_games = true
            round.pool = @pool
            round.start_date = @pool.start_date + (8*index).days
            round.end_date = @pool.start_date + (13*index).days
            round.deadline = @pool.start_date + (7*index).days
            round.save
            round_history = RoundHistory.new
            round_history.round = round
            round_history.save
          end
        end

        format.html { redirect_to pools_path,flash:
            {notice: 'Pool was successfully created.' ,success: true}}
        format.json { render action: 'show', status: :created, location: @pool }
      else
        format.html { render action: 'new', flash:
            {notice: 'Could not add pool.' ,success: false}}
        format.json { render json: @pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pools/1
  # PATCH/PUT /pools/1.json
  def update

    if params[:commit] == "Save Report"
      @pool.recap = params[:pool][:recap]
    else
      @pool.name = params[:pool][:name]
      @pool.league_id = params[:pool][:league_id].to_i
      @pool.cost = params[:pool][:cost].to_i
      @pool.prize = params[:pool][:prize].to_i
      @pool.number_of_tickets = params[:pool][:number_of_tickets].to_i
      @pool.max_user_credit = params[:pool][:max_user_credit].to_i
      @pool.start_date = datetime_from_params_hash(params[:pool], :start_date)
      @pool.end_date = datetime_from_params_hash(params[:pool], :end_date)
      @pool.deadline = datetime_from_params_hash(params[:pool], :deadline)
      @pool.overview = params[:pool][:overview]
    end

    respond_to do |format|
      if @pool.save
        format.html { redirect_to pools_path,flash:
            {notice: 'Pool successfully updated.' ,success: true} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit', flash:
            {notice: 'There was a problem updating the pool.  It could not updated' ,success: false} }
        format.json { render json: @pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pools/1
  # DELETE /pools/1.json
  def destroy
    @pool.destroy
    respond_to do |format|
      format.html { redirect_to pools_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pool
      @pool = Pool.find(params[:id])
      session[:pool_id] = @pool.id
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pool_params
      params.require(:pool).permit(:name, :prize, :cost, :number_of_rounds, :start_date, :deadline, :overview)
    end
    # Helper method that parses a datetime string hash elements and returns a datetime object
    def datetime_from_params_hash(hash, date_symbol)

      symbol_string = date_symbol.to_s

      DateTime.new(hash[symbol_string + '(1i)'].to_i,
                   hash[symbol_string + '(2i)'].to_i,
                   hash[symbol_string + '(3i)'].to_i,
                   hash[symbol_string + '(4i)'].to_i,
                   hash[symbol_string + '(5i)'].to_i)
    end


end
