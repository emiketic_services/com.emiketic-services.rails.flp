class RoundsController < ApplicationController
  before_action :set_round, only: [:history, :complete, :show, :edit, :update, :destroy, :display_stat]
  before_action :set_pool
  layout "backend"

  # GET /rounds
  # GET /rounds.json
  def index
    @rounds = Round.all
  end

  # GET /rounds/1
  # GET /rounds/1.json
  def show
  end

  # Shows round history
  def history

  end

  # Removes game from round and adds it to round history

  # GET /rounds/new
  # Initializes the form partial with the necessary data to create
  # a new round. Will be followed by a create action.
  def new
    @round = Round.new
    @new_game =  Game.new
    @teams = Team.find_by(league_id: @pool.league_id)
    @with_games = (params[:with_games] == "true") ? true : false
  end

  # GET /rounds/1/edit
  def edit
    @round = Round.find_by(id: params[:id])
    @pool = @round.pool
    @new_game = Game.new
    @teams = Team.where(league_id: @pool.league_id)
  end

  # POST /rounds
  # POST /rounds.json
  # This function is either called when admin saves a newly created round with an empty
  # game set in the _form partial, or when a game is added to a newly - still being edited - game.
  # In the latter, all the round attributes filled in the form fields above
  # (including nil/irrelevant values) will be saved. Saving the default game requires user has clicked
  # the 'Add and Save' button on the new game row. If user clicks the form 'Save' button, no games will
  # be added (games -> nil).
  # Newly created games have a score of -1 vs. -1 for convention reasons
  def create

    # Make sure we always get a clean object along with a default round history
    @round = Round.new
    @round_history = RoundHistory.new
    @round_history.round = @round

    # Set its attributes from parameters
    @round.name = params[:round][:name]
    @round.pool =  @pool
    @round.start_date = datetime_from_params_hash(params[:round], :start_date)
    @round.end_date = datetime_from_params_hash(params[:round], :end_date)
    @round.deadline = datetime_from_params_hash(params[:round], :deadline)
    @round.with_games = (params[:with_games] == "true") ? true : false

    # If submit request coming from a request to add a new game to an existing round

    if params[:add_game_to_new_round_button]
      add_game_to_round

      respond_to do |format|
        # Game creation and association building have the succeed first
        if @game.save

          # Then add the newly created game along with its team associations to the round games
          # associations hash
          @round.games << @game

          # Make sure saving succeeds and only then can we notify the user that operation succeeded
          if @round.save && @round_history.save
            format.html { redirect_to manage_pool_path(@pool),
                                      flash: {notice: 'Round and game were successfully created',
                                                                       success: true}}
            format.json { render action: 'show', status: :created, location: @round }

            # Redirect user to new round creation forms in both cases of error
          else
            format.html { render action: 'new' }
            format.json { render json: @round.errors, status: :unprocessable_entity }
          end

        else
          format.html { render action: 'new' }
          format.json { render json: @round.errors, status: :unprocessable_entity }
        end
      end

    # Create a round only (coming from the save button at the bottom left of the form)
    else
      if params[:save_existing_round_button]
        respond_to do |format|

          # Make sure saving succeeds and only then can we notify the user that operation succeeded
          if @round.save && @round_history.save
            format.html { redirect_to manage_pool_path(@pool),
                                      flash: {notice: 'Round was successfully created',
                                              success: true} }
            format.json { render action: 'show', status: :created, location: @round }

            # Redirect user to new round creation forms in both cases of error
          else
            format.html { render action: 'new' }
            format.json { render json: @round.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # PATCH/PUT /rounds/1
  # PATCH/PUT /rounds/1.json
  # Updates and existing round from the form fields in the _form partial. Just as the create action,
  # this action is either triggered by the round save submit button or by adding/saving
  # a game association.
  def update

    # Disable the check-mark flag in order for it to be used in pool/manage
    flash[:with_games] = nil

    # Set its attributes from parameters
    @round.name = params[:round][:name]
    @round.start_date = datetime_from_params_hash(params[:round], :start_date)
    @round.end_date = datetime_from_params_hash(params[:round], :end_date)
    @round.deadline = datetime_from_params_hash(params[:round], :deadline)

    # No support for with_games nor for pool_id updates as rounds type can not be changed by convention.
    # Only game and date details can be.


    # New Game to an Existing Round
    # If submit request coming from a request to add a new game to an existing round
    if params[:add_game_to_existing_round_button]

      # Invoke private method in charge of adding new games depending round existence
      add_game_to_round

      respond_to do |format|
        # Game creation and association building have the succeed first
        if @game.save

          # Then add the newly created game along with its team associations to the round games
          # associations hash
          @round.games << @game

          # Make sure saving succeeds and only then can we notify the user that operation succeeded
          if @round.save
            format.html { redirect_to edit_round_path(@round),
                                      flash: {notice: 'Successfully added game to round',
                                              success: true} }
            format.json { render action: 'show', status: :created, location: @round }

            # Redirect user to new round creation forms in both cases of error
          else
            format.html { redirect_to edit_round_path(@round),
                                      flash: {notice: 'Could not add game to round',
                                              success: false} }
            format.json { render json: @round.errors, status: :unprocessable_entity }
          end

        else
          format.html { redirect_to edit_round_path(@round),
                                    flash: {notice: 'Could update round - Error adding game',
                                            success: false} }
          format.json { render json: @round.errors, status: :unprocessable_entity }
        end
      end

      return

    end

                              #---------------------------------------#
                              # Updating round info and metadata only #

    if params[:save_existing_round_button]

      respond_to do |format|
        if @round.save
          format.html { redirect_to edit_round_path(@round),
                                    flash: {notice: 'Updated round info successfully',
                                            success: true} }
          format.json { head :no_content }
        else
          format.html { redirect_to edit_round_path(@round),
                                    flash: {notice: 'Could not update round info',
                                            success: false} }
          format.json { render json: @round.errors, status: :unprocessable_entity }
        end
      end

      # No need to proceed with the rest of this function
      return
    end


                                #------------------------------------#
                                # Updating existing round games only #

    # Otherwise, conventional model update (saving round or updating game) --  Look for game row first
    if (params[:update_existing_game_button]) ||
        (params[:remove_existing_game_button] ||
            (params[:keep_existing_game_button]))

      # Declare game hash iterator
      game_hash_iterator = nil
      should_save_game = false

      # Parse the param hash and get target game id, its first team id and second team id (if any)
      games_hash = params[:round][:games_attributes]

      if games_hash
        games_hash.each do |k,v|

          # Iterate over hash in search of a game row that is different from the DB records
          game_hash_iterator_id = params[:round][:games_attributes][k][:id].to_i
          game_hash_iterator = Game.find_by(id: game_hash_iterator_id)

          # If request coming from a Remove button, just remove game from round games
          # redirect and return
          if params[:remove_existing_game_button]

            @round.games.delete(game_hash_iterator)

            if @round.save
              respond_to do |format|
                format.html { redirect_to edit_round_path(@round),
                                          flash: {notice: 'Updated round game successfully - Game removed',
                                                  success: true} }
                format.json { head :no_content }
              end
            else
              respond_to do |format|
                format.html { redirect_to edit_round_path(@round),
                                          flash: {notice: 'Error updating round - Game was not removed',
                                                  success: false} }
                format.json { head :no_content }
              end
            end

            # No need to proceed with the rest of this function
            return
          end

          iterator_first_team_id = nil
          iterator_second_team_id = nil
          iterator_date = nil
          iterator_location = nil


          # Second team id if any
          if @round.with_games

            iterator_second_team_id = params[:round][:games_attributes][k][:second_team_id].to_i
            iterator_first_team_id = params[:round][:games_attributes][k][:first_team_id].to_i
            iterator_location = params[:round][:games_attributes][k][:location]
            iterator_date = datetime_from_params_hash(params[:round][:games_attributes][k], :date)

          # No second game or date/location for qualifications (this block was added to address FLP-287)
          # and was responsible for crashes following qualifications update
          else
            iterator_first_team_id = params[:round][:games_attributes][k][:first_team_id].to_i
            iterator_second_team_id = nil
          end


          if @round.with_games
            # For dual match games, detect any change to date, location or teams.
            if (iterator_first_team_id != game_hash_iterator.first_team_id) ||
                (iterator_second_team_id != game_hash_iterator.second_team_id) ||
                (iterator_location != game_hash_iterator.location) ||
                (iterator_date != game_hash_iterator.date)

              game_hash_iterator.first_team_id = iterator_first_team_id
              game_hash_iterator.second_team_id = iterator_second_team_id
              game_hash_iterator.location = iterator_location
              game_hash_iterator.date = iterator_date
              should_save_game = true
            end

            # For simple single match games, simply look for changes in teams
          else
            if iterator_first_team_id != game_hash_iterator.first_team_id
              game_hash_iterator.first_team_id = iterator_first_team_id
              game_hash_iterator.winner_id = iterator_first_team_id
              should_save_game = true
            end

            # When a Keep button is clicked, reset the winner_id
            # to the first team if game's winner id is nil (ie team is eliminated)
            if params[:keep_existing_game_button]
              if params[:clicked_game_id].to_i == game_hash_iterator.id
                game_hash_iterator.first_team_id = iterator_first_team_id
                game_hash_iterator.winner_id = iterator_first_team_id
                should_save_game = true
              end
            end

          end



          # No need to iterate more if change was found
          break if should_save_game
        end
      end

      # Apply changes and redirect with notice
      respond_to do |format|
        # Redirect after making sure the right buttons were clicked and fetched hash values are valid
        if ((params[:update_existing_game_button]) || (params[:remove_existing_game_button]) ||
            (params[:keep_existing_game_button])) &&
            ((game_hash_iterator) && (should_save_game))
          if game_hash_iterator && game_hash_iterator.save
            if @round.save
              format.html { redirect_to edit_round_path(@round),
                                        flash: {notice: 'Updated round game successfully',
                                                success: true} }
              format.json { head :no_content }
            else
              format.html { redirect_to edit_round_path(@round),
                                        flash: {notice: 'Could not update game - round saving error',
                                                success: false} }
              format.json { render json: @round.errors, status: :unprocessable_entity }
            end
          else
            format.html { redirect_to edit_round_path(@round),
                                      flash: {notice: 'Could not update round game - game saving error',
                                              success: false} }
            format.json { render json: game_hash_iterator.errors, status: :unprocessable_entity }
          end
        else
          format.html { redirect_to edit_round_path(@round),
                                    flash: {notice: 'Could not update round game - no games matching',
                                            success: false} }
          format.json { render json: game_hash_iterator.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def update_game

  end

  def complete

    # Find all the winners in the round
    round_winners = []
    @round.games.each do |game|
      unless round_winners.include?(game.winner_id) || game.winner_id.nil?
        round_winners << game.winner_id
      end
    end

    # For every ticket in the round, see their pick and see if it is found in the winners collection
    @round.tickets.each_with_index do |ticket|
      # Make sure ticket has made a pick in this round
      if ticket.picks.where(round_id: @round.id).any?
        if round_winners.include?(ticket.picks.where(round_id: @round.id).first.team_id)
          # Move ticket to the next round
          unless Round.all.order(:start_date).index(@round) == Round.count - 1
            ticket.round_id = Round.all.order(:start_date).at(Round.all.order(:start_date).index(@round) + 1).id
          end
        else
          ticket.validity = false

        end
        if ticket.save
          # Notify user of her elimination once ticket elimination is commited to the database
          unless ticket.validity
            #UserNotifier.eliminated(ticket).deliver
          end
        end
      end

    end

    # Mark round as completed
    @round.complete = true

    if @round.save
      respond_to do |format|
        format.html {redirect_to manage_pool_path(@pool), flash: {notice: 'Round completed',
                                                                     success: true}}
        format.json
      end
    else
      respond_to do |format|
        format.html {redirect_to manage_pool_path(@pool), flash: {notice: 'Could not complete round',
                                                                     success: true}}
        format.json
      end
    end


  end

  def display_stat
    success_notice_message =
        @round.display_stat ? 'Stats are now hidden from users in dashboard' :
            'Stats are now published to users in dashboard'
    error_notice_message =
        @round.display_stat ? 'Could not hide stats from users in dashboard' :
            'Could not publish stats to users on in dashboard'
    @round.display_stat = !@round.display_stat

    if @round.save
      respond_to do |format|
        format.html {redirect_to manage_pool_path(@round.pool),
                                 flash:{notice: success_notice_message,
                                        success: true}}
      end
    else
      respond_to do |format|
        format.html {redirect_to manage_pool_path(@round.pool),
                                 flash:{notice: error_notice_message,
                                        success: true}}
      end
    end

  end



  # DELETE /rounds/1
  # DELETE /rounds/1.json
  def destroy
    pool = @round.pool
    @round.destroy
    respond_to do |format|
      format.html { redirect_to manage_pool_path(pool) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round
      @round = Round.find(params[:id])
    end

    # Presets pool from session before calling any method
    def set_pool
      @pool = Pool.find_by(id: session[:pool_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_params
      params.require(:round).permit(:start_date, :end_date, :deadline, :with_games)
    end

    # Invoked by update when the button Add and Save (with a new game to an existing round)
    # was triggered
    def add_game_to_round

      #  For existing rounds with dual match games  OR new rounds
      # being created with the same conditions
      if (@round.with_games) || (params[:round][:with_games])

        # Build association based on the game ids passed to the params
        game_date = datetime_from_params_hash(params[:round][:game], :date)
        game_location = params[:round][:game][:location]
        default_score = -1
        @game = Game.new(date: game_date,
                         location: game_location,
                         first_team_score: default_score,
                         second_team_score: default_score,
                         tie: false)
        @game.first_team_id = params[:round][:game][:first_team_id].to_i
        @game.second_team_id =  params[:round][:game][:second_team_id].to_i

        # Add a league reference to game for ulterior practical reasons (unused column in )
        @game.league_id = @pool.league_id

      # For existing or new rounds without dual match games
      else


        # Assign default round deadline to game date and nil location
        game_date = (@round.new_record?) ? params[:round][:deadline] : @round.deadline
        game_location = nil
        default_score = -1
        @game = Game.new(date: game_date,
                         location: game_location,
                         first_team_score: default_score,
                         second_team_score: default_score,
                         tie: false)
        @game.first_team_id = params[:round][:game][:first_team_id].to_i
        @game.winner_id = params[:round][:game][:first_team_id].to_i
      end

    end

    # Helper method that parses a datetime string hash elements and returns a datetime object
    def datetime_from_params_hash(hash, date_symbol)

      symbol_string = date_symbol.to_s

      DateTime.new(hash[symbol_string + '(1i)'].to_i,
                   hash[symbol_string + '(2i)'].to_i,
                   hash[symbol_string + '(3i)'].to_i,
                   hash[symbol_string + '(4i)'].to_i,
                   hash[symbol_string + '(5i)'].to_i).in_time_zone('Eastern Time (US & Canada)')
    end
end
