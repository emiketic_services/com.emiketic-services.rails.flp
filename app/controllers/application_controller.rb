class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authorize

  protected

  def authorize
    unless session[:admin_logged]
      redirect_to '/admin', method: :get, flash: {notice: 'Please login to admin panel', success: false}
    end
  end

end
