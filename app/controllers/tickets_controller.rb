class TicketsController < ApplicationController
  skip_before_action :authorize, only: [:new, :create, :update, :destroy]
  before_action :set_ticket, only: [:show, :edit, :update, :destroy, :disable]
  layout "backend"

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ticket }
      else
        format.html { render action: 'new' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    managed_user = @ticket.user
    @ticket.destroy
    respond_to do |format|
      if session[:admin_logged]
        format.html { redirect_to manage_user_path(managed_user) }
        format.json { head :no_content }
      else
        session[:user_id] = managed_user.id
        format.html { redirect_to dashboard_my_tickets_path }
        format.json { head :no_content }
      end

    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def disable
    managed_user = @ticket.user
    @ticket.validity = !@ticket.validity
    if @ticket.save!
      respond_to do |format|
        if session[:admin_logged]
          format.html { redirect_to manage_user_path(managed_user) }
          format.json { head :no_content }
        else
          session[:user_id] = managed_user.id
          format.html { redirect_to dashboard_my_tickets_path }
          format.json { head :no_content }
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:value, :date_of_sale, :expiry_date, :valid)
    end
end
