module UsersHelper

  def past_picks(ticket)

    # Past picks
    past_picks = []
    if ticket
      unless ticket.picks.count == 0
        ticket.picks.order(:created_at).each do |past_pick|
          unless past_pick.round == ticket.round
            past_picks << past_pick.team_id
          end
        end
      end

    end

    return past_picks

  end

  def possible_picks(round, ticket)
      # Past picks
      past_picks = past_picks(ticket)

      possible_picks = []

      # Completed round pick
      if ticket.validity
        if round.games.present?

          # For every game in round
          round.games.each do |game|

            # Make sure first team association id is not present in the possible_picks array
            # Also make sure doesn't select the same team twice for a survival ticket
            unless possible_picks.include?(game.first_team_id) || past_picks.include?(game.first_team_id)
              possible_picks << Team.find(game.first_team_id)
            end

            # Then, add the second team if it doesn't exist neither for the case of dual-game based rounds
            if round.with_games
              unless possible_picks.include?(game.second_team_id)|| past_picks.include?(game.second_team_id)
                possible_picks << Team.find(game.second_team_id)
              end
            end
          end

        end
      else
        possible_picks = nil
      end
      return possible_picks
  end

end
