module DashboardHelper

  def has_tickets_without_picks(pool)
    ticket_without_picks = false
    pool.tickets.each do |ticket|
      pool.rounds.each do |round|
        unless ticket.picks.where(round: round).any?
          ticket_without_picks = true
          break
        end
      end
    end
    return ticket_without_picks
  end
end
