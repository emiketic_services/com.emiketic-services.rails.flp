/**
 * Created by MehdiMahmoudi on 4/15/14.
 */

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks

function handle_selection(selector_name, selector_page, select_tag) {

    if (selector_page == "my_pools") {
        if (selector_name == "pools") {
            document.location.href = "/dashboard/my_pools/" + select_tag.selectedIndex;
        }

        if (selector_name == "tickets") {
            document.location.href = "/dashboard/my_pools/ticket/" + (select_tag.selectedIndex + 1);
        }
    }
    if (selector_page == "overview") {
            if (selector_name == "pools") {
                document.location.href = "/dashboard/overview/pool/" + select_tag.selectedIndex;
            }

            if (selector_name == "tickets") {
                document.location.href = "/dashboard/overview/ticket/" + (select_tag.selectedIndex+1);
            }
            if (selector_name == "rounds") {
                document.location.href = "/dashboard/overview/round/" + (select_tag.selectedIndex+1);
            }
        }
    if (selector_page == "schedule") {
            if (selector_name == "pools") {
                document.location.href = "/dashboard/schedule/" + select_tag.selectedIndex;
            }

            if (selector_name == "tickets") {
                document.location.href = "/dashboard/schedule/ticket/" + (select_tag.selectedIndex+1);
            }

            if (selector_name == "rounds") {
                document.location.href = "/dashboard/stat_recap/round/" + (select_tag.selectedIndex+1);
            }
        }
    if (selector_page == "stat_recap") {
            if (selector_name == "pools") {
                document.location.href = "/dashboard/stat_recap/pool/" + select_tag.selectedIndex;
            }

            if (selector_name == "tickets") {
                document.location.href = "/dashboard/stat_recap/ticket/" + (select_tag.selectedIndex+1);
            }

            if (selector_name == "rounds") {
                document.location.href = "/dashboard/stat_recap/round/" + (select_tag.selectedIndex+1);
            }
    }

}
