class UserNotifier < ActionMailer::Base
  default from: "Fat Lady Pools <info@fatladypools.com>",
          bcc: ["fatladypools@gmail.com", "logs@fatladypools.com"],
  	  reply_to: "info@fatladypools.com"

  # Email A
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.registered.subject
  #
  def registered(user)
    @user = user

    mail to: user.email, subject: "Welcome to Fat Lady Pools!"
  end

  def reset_password(user)
    @user = user

    mail to: user.email, subject: "Fat Lady Pools - Reset your Password"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.registered_ticket.subject
  #
  def registered_ticket(user, ticket)
    @user = user
    @pool_name = ticket.pool.name

    mail to: user.email, subject: "Fat Lady Pools - Pool Registration"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.picked.subject
  #
  def picked(user, pick)
    @user = user
    @pick_name = Team.find_by(id: pick.team_id).name
    @ticket = pick.ticket
    @ticket_number = @user.tickets.order(:created_at).index(@ticket) + 1
    @round = @ticket.round

    mail to: user.email, subject: "Fat Lady Pools - Your Last Pick"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.round_deadline_reminder.subject
  #
  def round_deadline_reminder(user, round)
    @user = user
    @round = round

    mail to: "to@example.org"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.pool_deadline_reminder.subject
  #
  def pool_deadline_reminder
    @greeting = "Hi"

    mail to: "to@example.org"
  end

  # Email B
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.deposit.subject
  #
  def deposit(user)
    @user = user
    mail to: user.email, subject: "Fat Lady Pools - Deposit Confirmation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.eliminated.subject
  #
  def eliminated(ticket)
    @user= ticket.user

    mail to: @user.email, subject: "The fat lady has sung"
  end

  def mail_tester
    mail to: 'mehdi@appid.tn', from: 'info@fatladypools.com', subject:'FLP Mailer Test'
  end

end
