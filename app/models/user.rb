# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  username   :string(255)
#  password   :string(255)
#  first_name :string(255)
#  last_name  :string(255)
#  email      :string(255)
#  phone      :string(255)
#  country    :string(255)
#  province   :string(255)
#  dob        :date
#  created_at :datetime
#  updated_at :datetime
#  balance    :integer
#

class User < ActiveRecord::Base

  validates :first_name, :last_name, :username, :email, :phone, :password, presence: true
  validates :password, format: {with: /\A(?=.*[a-zA-Z])(?=.*[0-9]).{6,}\Z/,
                                message: "password must be at least 6 characters long and include one number and one letter."}
  validates_confirmation_of :password
  validates :email, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create }
  validates :username, :email, uniqueness: true
  #validate :dob_must_yield_minimum_age

  has_many :tickets, dependent: :destroy
  has_many :pools, through: :tickets, uniq: true

  def compute_age
    (dob + 18.years) < Date.today
  end

  def dob_must_yield_minimum_age
    errors.add(:dob, "Date of birth is invalid. You have to be at least 18 years old.") unless compute_age
  end

end
