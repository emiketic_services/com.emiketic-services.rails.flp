# == Schema Information
#
# Table name: leagues
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  sport_id   :integer
#

class League < ActiveRecord::Base
  has_many :pools, dependent: :destroy
  has_many :teams, dependent: :destroy
  belongs_to :sport
end
