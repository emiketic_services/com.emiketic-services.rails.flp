class PoolHistory < ActiveRecord::Base

  has_many :tickets, through: :pools
  has_many :rounds, through: :pools

end
