class RoundHistory < ActiveRecord::Base

  belongs_to :round
  has_many :games, through: :round
  has_many :tickets, through: :round
end
