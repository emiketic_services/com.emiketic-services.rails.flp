# == Schema Information
#
# Table name: teams
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  game_id    :integer
#  league_id  :integer
#

class Team < ActiveRecord::Base
  belongs_to :game
  belongs_to :league
  has_many :picks
end
