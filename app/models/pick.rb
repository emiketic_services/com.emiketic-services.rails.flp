# == Schema Information
#
# Table name: picks
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  ticket_id  :integer
#  game_id    :integer
#

class Pick < ActiveRecord::Base
  belongs_to :game
  belongs_to :ticket
  belongs_to :team
  belongs_to :round

end
