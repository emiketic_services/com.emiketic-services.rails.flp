# == Schema Information
#
# Table name: rounds
#
#  id         :integer          not null, primary key
#  start_date :datetime
#  end_date   :datetime
#  deadline   :datetime
#  with_games :boolean
#  created_at :datetime
#  updated_at :datetime
#  pool_id    :integer
#  name       :string(255)
#

class Round < ActiveRecord::Base

  belongs_to :pool
  has_many :tickets
  has_many :games, dependent: :destroy # Games destroy picks when they're destroyed
  has_many :picks
  has_one :round_history
  accepts_nested_attributes_for :games
end
