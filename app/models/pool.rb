# == Schema Information
#
# Table name: pools
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  prize            :integer
#  cost             :integer
#  number_of_rounds :integer
#  start_date       :datetime
#  deadline         :datetime
#  overview         :text
#  created_at       :datetime
#  updated_at       :datetime
#  league_id        :integer
#  end_date         :datetime
#  recap            :text
#

class Pool < ActiveRecord::Base
  validates :number_of_tickets, :number_of_rounds, :cost, :prize, :max_user_credit, presence: true
  validates :number_of_rounds, :max_user_credit,
            numericality: {greater_than_or_equal_to: 0}
  validates :number_of_tickets, :cost, :prize,
            numericality: {greater_than_or_equal_to: 1}
  
  has_many :rounds, dependent: :destroy
  has_many :tickets, dependent: :destroy
  belongs_to :league
end
