# == Schema Information
#
# Table name: sports
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Sport < ActiveRecord::Base
  has_many :leagues, dependent: :destroy
end
