# == Schema Information
#
# Table name: tickets
#
#  id           :integer          not null, primary key
#  value        :integer
#  date_of_sale :datetime
#  expiry_date  :datetime
#  created_at   :datetime
#  updated_at   :datetime
#  pool_id      :integer
#  user_id      :integer
#  round_id     :integer
#  validity     :boolean
#

class Ticket < ActiveRecord::Base
  belongs_to :pool
  belongs_to :user
  belongs_to :round
  has_many :picks, dependent: :destroy
end
