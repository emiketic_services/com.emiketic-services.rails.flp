# == Schema Information
#
# Table name: games
#
#  id                :integer          not null, primary key
#  date              :datetime
#  location          :string(255)
#  first_team_score  :integer
#  second_team_score :integer
#  tie               :boolean
#  created_at        :datetime
#  updated_at        :datetime
#  round_id          :integer
#  pick_id           :integer
#  first_team_id     :integer
#  second_team_id    :integer
#  winner_id         :integer
#  league_id         :integer
#

class Game < ActiveRecord::Base
  validates :first_team_score, presence: true, numericality:{only_integer: true}
  validates :second_team_score, presence: true, numericality:{only_integer: true}

  belongs_to :round
  belongs_to :league
  has_many :picks, dependent: :destroy # Games are destoryed by rounds
  has_one :first_team, class_name: "Team", foreign_key: "id"
  has_one :second_team, class_name: "Team", foreign_key: "id"
  has_one :winner, class_name: "Team", foreign_key: "id"
end
